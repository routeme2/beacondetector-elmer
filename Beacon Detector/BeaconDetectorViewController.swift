//
//  ViewController.swift
//  Beacon Locator
//
//  Created by Elmer Orellana on 4/25/17.
//  In collaboration with Alice Aimee Alvarado
//  Copyright © 2017 RouteMe2. All rights reserved.
//

import UIKit
import CoreLocation
import Foundation

class BeaconDetectorViewController: UIViewController, CLLocationManagerDelegate {
    
    
/************************************* Var and Labeles ****************************************************/
    // Select region to monitor our own iBeacons from kontakt.io
    // by using our UUID and identifing them as kontakt beacons
    // For monitoring a specific iBeacon include that iBeacon's major and minor
    let regionToMonitor = CLBeaconRegion(proximityUUID: UUID(uuidString:"F7826DA6-4FA2-4E98-8024-BC5B71E0893E")!, major:9073, minor:1, identifier: "kontakt beacons")
    
    // Declare a location Manager that coordinates all locations
    let locationManager = CLLocationManager()
    
    // Globle variable for file's name that will contain the data collected
    var filename:String = ""
    
    
    // Iterate through the list of iBeacons and display their properties on the label
    // String is also used to gather indiviual iBeacon info depending on the region to monitor above
    //var beaconInfo:String = "Dist,RSSI,TX\n"
    
    
    // Create an instance of Timer
    var timer = Timer()
    
    // Will hold the value of iBeacon's RSSIs as they are detected
    var iBeaconRSSI = 0
    
    // Array of RSSI values with respect to distance
    var RSSIArray = [[]]
    
    // Will temporarely hold the average of the measurements at each distance interval
    var iBeaconRSSIMean:Double = 0.0
    
    // Will hold the calculated averages
    var RSSIMeanArray = [Double]()
    
    // Will temporarely hold the calculated variances of each measurement at a given distance
    var iBeaconRSSIVariance:Double = 0.0
    
    // Will temporarely hold the calculated standard diviation of a given set of measurements
    var iBeaconRSSIStdDiv:Double = 0.0
    
    // Will hold the calculated standard diviations of each measurement at a given distance
    var RSSIStdDivArray = [Double]()
    
    // Will temporarely hold the calculated Signal-to-Noise ratio
    var iBeaconSNR:Double = 0.0
    
    // Will hold the calculated Signal-to-Noise ration for each measurement at a given distance
    var RSSISNRArray = [Double]()
    
    // this file will have the beacon information to write to file
    var writeString:String = ""
    
    // Will temporarely hold the upper bound of the confidence threshold
    var RSSIConfidenceUpper = 0.0
    
    // Will hold all the upper bound confidence threshold for the RSSI
    var RSSIConfidenceUpperArray = [Double]()
    
    // Will temporarely hold the lower bound of the confidence threshold
    var RSSIConfidenceLower = 0.0
    
    // Will hold all the lower bound confidence threshold for the RSSI
    var RSSIConfidenceLowerArray = [Double]()
    
    // Will hold the number of packets detected at any given distance
    var packetCount = 0
    
    // Will hold all the packet counts in the array
    var packetCountArray = [Int]()
    
    
/************************************** IBOutlets *********************************************************/
    
    // Represents the name of the beacon. See label on actual beacon
    @IBOutlet weak var beaconName: UITextField!
    
    // Enter the distance from the iBeacon in meters
    @IBOutlet weak var distanceInput: UITextField!
    
    // Enter the Tx value that was set in the iBeacon
    @IBOutlet weak var txValue: UITextField!
    
    // Chose where measurements are taken
    // ON for indoor and OFF for outdoor
    @IBOutlet weak var indoorMode: UISwitch!
    
    // Choose if beacon is broadcasint Eddystone
    @IBOutlet weak var EddystoneSwitch: UISwitch!
    
    // Enter the amount of time to scan in seconds
    @IBOutlet weak var scanTimer: UITextField!
    
    // Will display the beacon information of the beacon
    @IBOutlet weak var beaconLabel: UILabel!

/*************************************** IBActions ********************************************************/
    
    
    // Scan for beacons button
    @IBAction func scanForBeacons(_ sender: Any) {
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(BeaconDetectorViewController.scanForBeaconsHelper), userInfo: nil, repeats: true)
        
    }
    
    // scanForBeacons helper function
    
    var globalCounter = 0
    func scanForBeaconsHelper(){
        if globalCounter < Int(scanTimer.text!)!{
            
            print("GlobalCounter: \(globalCounter), RSSI: \(iBeaconRSSI)")
            beaconLabel.text = "GlobalCounter: " + String(globalCounter + 1) + " RSSI: " + String(iBeaconRSSI)
            
            
            // For debugging purposes
                RSSIArray[Int(distanceInput.text!)!].append(iBeaconRSSI)
                print(RSSIArray)
                print(RSSIArray.count)
                globalCounter += 1
            
        }else{
            
            // Stop timer and clear globalCounter
            timer.invalidate()
            globalCounter = 0
            
            // Calculate the average of the recently added values
            iBeaconRSSIMean = getAverage(array: RSSIArray[Int(distanceInput.text!)!] as! [Int])
            
            // Calculate the variance of the recently added values
            iBeaconRSSIVariance = getVariance(array: RSSIArray[Int(distanceInput.text!)!] as! [Int], mean: iBeaconRSSIMean)
            
            // Calculate the standard diviation
            // Standard diviation is the square root of the variance
            iBeaconRSSIStdDiv = sqrt(iBeaconRSSIVariance)
            
            
            // Calculate the confidence threshold upper bound
            RSSIConfidenceUpper = iBeaconRSSIMean + iBeaconRSSIStdDiv
            
            // Calculate the confidence threshold lower bound
            RSSIConfidenceLower = iBeaconRSSIMean - iBeaconRSSIStdDiv
            
            // Calculate Signal-to-Noise ratio
            // SNR = mean/(standard deviation)
            // Avoid division by zero
            if iBeaconRSSIStdDiv == 0{
                iBeaconSNR = 200
            }else{
                iBeaconSNR = abs(iBeaconRSSIMean/iBeaconRSSIStdDiv)
            }
            
            
            // Add the average to RSSIMeanArray
            RSSIMeanArray.append(iBeaconRSSIMean)
            
            // Add the standard diviation to RSSIStdDivArray
            RSSIStdDivArray.append(iBeaconRSSIStdDiv)
            
            // Add the Signal-to-Noise ratio to the RSSISNRArray
            RSSISNRArray.append(iBeaconSNR)
            
            // Add a new column to the RSSIConfidenceUpperArray
            RSSIConfidenceUpperArray.append(RSSIConfidenceUpper)
            
            // Add a new column to the RSSIConfidenceLowerArray
            RSSIConfidenceLowerArray.append(RSSIConfidenceLower)
            
            // Count the number of packets received per second
            packetCount = getPacketCount(array: RSSIArray[Int(distanceInput.text!)!] as! [Int])
            
            // Add the count to the packetCountArray array
            packetCountArray.append(packetCount)
    
            // Add a new column to the RSSIarray
            RSSIArray.append([])
            
            // Signal completion of scan
            beaconLabel.text = "Scan Complete"
            
            // for debugging purposes
            print("Summay")
            print(RSSIArray)
            print("RSSIMeanArray \(RSSIMeanArray)")
            print("RSSIStdDivArray \(RSSIStdDivArray)")
            print("RSSI SNR \(RSSISNRArray)")
        }
    }
    
    // Store Data button
    @IBAction func storeData(_ sender: Any) {

   //////////////////////////////////////Use this code to calculate SNR/////////////////////////////////////////
       
        // Prepare the information that will be written in text file
  //      writeString = "Distance (m),RSSI Mean,RSSI Std Dev,Confidence Upper,Confidence Lower,SNR\n"
  //      for i in 0..<RSSIMeanArray.count{
  //          writeString = writeString + String(i) + "," + String(RSSIMeanArray[i]) + "," + String(RSSIStdDivArray[i]) + "," + String(RSSIConfidenceUpperArray[i]) + "," + String(RSSIConfidenceLowerArray[i])+"," + String(RSSISNRArray[i]) + "\n"
  //      }
        
  //////////// Use this code to compare broadcast difference between iBeacon alone versus both iBeacon and ////
  //////////// Eddystone simultaneously ///////////////////////////////////////////////////////////////////////
        
        // Prepare the information that will be written in text file
        writeString = "Distance (m),number of packets\n"
        for i in 0..<packetCountArray.count{
            writeString = writeString + String(i) + "," + String(packetCountArray[i]) + "\n"
        }
        
 /////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        
        // Stop looking for iBeacons
        locationManager.stopRangingBeacons(in: regionToMonitor)
        
        //Locate the document directory and save the data there
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let dirFilePath = dir.appendingPathComponent(filename)
            
            //Save the file
            do {
                try writeString.write(to: dirFilePath, atomically: false, encoding: String.Encoding.utf8)
                
                // Reports back to the tester immediate
                beaconLabel.text = "Data Stored!"
                
            } catch _ {
                
                // Reports back to the tester immediate
                beaconLabel.text = "Data Not Stored!"
                
                print("Could not save the file.")
            }
        }

    }
/**********************************************************************************************************/
    
    
/**************************************** My functions ****************************************************/

//////////////////////// This function calculates the average of a given int array /////////////////////////
    
    func getAverage(array: [Int]) -> Double{
        
        var total = 0.0
        for item in array{
            total += Double(item)
        }
        return total/Double(array.count)
    }
    
////////////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////// This function calculates the variance of a set of values ///////////////////////////
    
    func getVariance(array: [Int], mean: Double) -> Double{
     
        var diffSqrSum = 0.0
        for index in 0..<array.count{
            let difference = Double(array[index]) - mean
            let diffSqr = difference*difference
            diffSqrSum += diffSqr
        }
        return diffSqrSum/Double(array.count)
    }
    
////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
///////////////////// This function calculates the number of received packets //////////////////////////////
    
    func getPacketCount(array: [Int]) -> Int{
    
        var total = 0
        for item in array{
            
            if item != 0{
                total+=1
            }
        }
         return total
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
/**********************************************************************************************************/
    

/************************************ System functions ****************************************************/

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
///////////////////////////////// Tap anywhere to hide keyboard ////////////////////////////////////////////
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BeaconDetectorViewController.dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        tap.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)
    }
    
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
        
////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
////////////////////////////////////  Indoor and Eddystone switches ////////////////////////////////////////
       
        // Indoor/Outdoor selector
        var iBeaconIndoor = ""
        if(indoorMode.isOn){
            iBeaconIndoor = "Indoor"
        }else{
            iBeaconIndoor = "Outdoor"
        }
        
        // Eddystone Selector
        var EddystoneOn = ""
        if(EddystoneSwitch.isOn){
            EddystoneOn = "EddystoneON"
        }else{
            EddystoneOn = "iBeaconOnly"
        }
        
////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
//////////////// locationManager delegate declaration and request for location authorization ///////////////
        

        // Let this class receive location updates
        locationManager.delegate = self
        
        // Request authorization to use location services. Manually done in iOS settings.
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedWhenInUse){
            locationManager.requestWhenInUseAuthorization()
        }
        
        // Begin monitoring
        locationManager.startRangingBeacons(in: regionToMonitor)
        
////////////////////////////// Create a unique file name ///////////////////////////////////////////////////
        
        //Create a file name that is unique
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMddyyyy-hhmmss"
        let dateStringFormat = dayTimePeriodFormatter.string(from: Date())
        
        filename = "\(iBeaconIndoor)\(EddystoneOn)\(scanTimer.text!)sTX\(txValue.text!)\(beaconName.text!)\(dateStringFormat).txt"
    }
    
////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
/*********************************** Location Manager Function ********************************************/
    
    // Receive iBeacon updates on their current informatiom
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {

        
        // Grabs iBeacons' information wanted
        for beacon in beacons {

            iBeaconRSSI = beacon.rssi
            
        }
    }
/**********************************************************************************************************/
    
}// End of class

