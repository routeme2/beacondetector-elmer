//
//  GPSLocalizationViewController.swift
//  Beacon Detector
//
//  Created by Elmer Orellana on 3/31/17.
//  Copyright © 2017 RouteMe2. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class GPSLocalizationViewController: UIViewController, CLLocationManagerDelegate {
    
    // Declare a location Manager that coordinates all locations
    let locationManager = CLLocationManager()

    @IBOutlet weak var LatitudeLabel: UILabel!

    @IBOutlet weak var LongitudeLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // Let this class receive location updates
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        // Request authorization to use location services. Manually done in iOS settings.
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedWhenInUse){
            locationManager.requestWhenInUseAuthorization()
        }
        
        locationManager.startUpdatingLocation()
        
       // LatitudeLabel.text =  "Latitude: \((locationManager.location?.coordinate.latitude)!)"
       // LongitudeLabel.text = "Longitude: \((locationManager.location?.coordinate.longitude)!)"
        
    }
    
    //Map
    @IBOutlet weak var map: MKMapView!
    
    

    @IBAction func StopUpdatingLocationButton(_ sender: UIButton) {
        
        locationManager.stopUpdatingLocation()
        
    }
    
    
    
    // Receive iBeacon updates on their current informatiom
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        
        let span:MKCoordinateSpan = MKCoordinateSpanMake(0.05, 0.05)
        
        let myLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
        
        let region:MKCoordinateRegion = MKCoordinateRegionMake(myLocation, span)
        
        map.setRegion(region, animated: true)
        
        self.map.showsUserLocation = true
    
        let accuracyCircle = MKCircle(center: myLocation, radius: location.horizontalAccuracy)
        
        accuracyCircle.title = "GPS accuracy"
        accuracyCircle.subtitle = "\(location.horizontalAccuracy)"
        
        map.addAnnotation(accuracyCircle)
        
        LatitudeLabel.text =  "Latitude: \(location.coordinate.latitude)"
        LongitudeLabel.text = "Longitude: \(location.coordinate.longitude)"
    
        ///////Here is where I left off///
        
    }
}
